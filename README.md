# YouTube Jumpcutter

Speeds up YouTube videos using the [jumpcutter algorithm](https://github.com/carykh/jumpcutter) by [carykh](https://github.com/carykh).

At the moment, the extension is only really good at either completely removing or only including silence instead of changing it's speed like in the original algorithm. Altering a video's speed accurately requires fine-grained control over the frames of the video which is difficult to achieve without tanking performance.
