import * as dat from 'dat.gui';

const YouTubeAudio = {
  getCurrentVideoId() {
    const parsedUrl = new URL(document.baseURI);
    const matches = parsedUrl.search.match(/\?v=(.+)&*|&/)
    if (matches) {
      return matches[1];
    }
    return null;
  },

  parseQueryString(str) {
    return str.split('&').reduce(function (params, param) {
      var paramSplit = param.split('=').map(function (value) {
        return decodeURIComponent(value.replace('+', ' '));
      });
      params[paramSplit[0]] = paramSplit[1];
      return params;
    }, {});
  },

  getProxyUrl(videoId, url) {
    return `https://${videoId}-focus-opensocial.googleusercontent.com/gadgets/proxy?container=none&url=${encodeURIComponent(url)}`;
  },

  async getVideoMetadata(videoId) {
    const url = `https://www.youtube.com/get_video_info?video_id=${videoId}`;
    // const url = "https://" + videoId + "-focus-opensocial.googleusercontent.com/gadgets/proxy?container=none&url=https%3A%2F%2Fwww.youtube.com%2Fget_video_info%3Fvideo_id%3D" + videoId;
    const response = await fetch(url);
    // TODO: Handle non-OK response
    const formDataString = await response.text();
    return YouTubeAudio.parseQueryString(formDataString);
  },

  async getAudioStreams(videoId) {
    const audioStreams = {};
    const metadata = await YouTubeAudio.getVideoMetadata(videoId);
    const streams = (metadata.url_encoded_fmt_stream_map + ',' + metadata.adaptive_fmts).split(',');
    streams.forEach((s) => {
      const stream = YouTubeAudio.parseQueryString(s);
      const itag = stream.itag * 1;
      let quality = '';
      switch (itag) {
        case 139:
          quality = "48kbps";
          break;
        case 140:
          quality = "128kbps";
          break;
        case 141:
          quality = "256kbps";
          break;
      }
      if (quality) audioStreams[quality] = stream.url;
    });
    return audioStreams;
  },

  chooseAudioStream(audioStreams, best = true) {
    let qualities = ['256kbps', '128kbps', '48kbps'];
    if (!best) {
      qualities = qualities.reverse();
    }
    for (const quality of qualities) {
      if (quality in audioStreams) {
        return audioStreams[quality];
      }
    }
    return null;
  },

  async getAudioUrlOfCurrentVideo() {
    const videoId = YouTubeAudio.getCurrentVideoId();
    console.log(`Fetching ${videoId} audio URL...`);
    const audioStreams = await YouTubeAudio.getAudioStreams(videoId);
    return YouTubeAudio.chooseAudioStream(audioStreams, false);
  },
}

/**
 * 
 * @param {AudioBuffer} buffer 
 * @param {*} binSize 
 */
// FIXME: Improve performance
function getBins(buffer, binSize, bothChannels = false) {
  console.time('getBins');
  let channels = bothChannels ? buffer.numberOfChannels : 1;
  let channelData = [];
  for (let channel = 0; channel < channels; channel++) {
    channelData[channel] = buffer.getChannelData(channel);
  }
  let count = 0;
  let binMax = 0;
  const peaks = [];
  for (let i = 0; i < buffer.length; i++) {
    for (let channel = 0; channel < channelData.length; channel++) {
      const v = channelData[channel][i];
      if (v > binMax) {
        binMax = v;
      }
    }
    count++;
    if (count > binSize) {
      peaks.push(binMax);
      count = 0;
      binMax = 0;
    }
  }
  console.timeEnd('getBins');
  return peaks;
}

// TODO: Make this smart enough to only draw what's visible
class LineGraph {
  /**
   *
   * @param {CanvasRenderingContext2D} ctx
   * @param {number} worldWidth
   * @param {number} worldHeight
   * @param {number[]} values
   * @param {((value) => string)|string} strokeStyle
   */
  constructor(ctx, worldWidth, worldHeight, values, strokeStyle = '#fff') {
    this.ctx = ctx;
    // TODO: Give this names more applicable to audio instead of games
    this.worldWidth = worldWidth;
    this.worldHeight = worldHeight;
    this.values = values;
    this.strokeStyle = strokeStyle;
    this.position = {
      x: 0,
      y: 0,
    };
  }

  render() {
    // Multiply by 1.0 to convert to float
    const sliceWidth = this.worldWidth * 1.0 / this.values.length;
    let x = 0;
    let lastX = 0;
    let lastY = 0;
    if (typeof this.strokeStyle !== 'function') {
      this.ctx.strokeStyle = this.strokeStyle;
    }
    let valueOffset = Math.floor((-this.position.x) / sliceWidth);
    /* if (valueOffset < 0) {
      valueOffset = 0;
    } */
    let slicesInFrame = Math.floor(this.ctx.canvas.width * 1.0 / sliceWidth);
    for (let i = 0; i < slicesInFrame; i++) {
      const value = this.values[valueOffset + i];
      const y = this.worldHeight - (value * this.worldHeight);
      if (typeof this.strokeStyle === 'function') {
        this.ctx.fillStyle = this.strokeStyle(value);
      }
      this.ctx.save();
      this.ctx.translate(this.position.x % sliceWidth, 0);
      this.ctx.fillRect(x, y, x - lastX, this.ctx.canvas.height);
      /* this.ctx.beginPath();
      this.ctx.moveTo(lastX, lastY);
      this.ctx.lineTo(x, y);
      this.ctx.stroke(); */
      this.ctx.restore();
      lastX = x;
      lastY = y;
      x += sliceWidth;
      if (x > this.ctx.canvas.width) {
        break;
      }
    }
  }
}

function getChunk(percentage, chunks) {
  const chunkNumber = Math.floor(percentage * chunks.length);
  const chunk = chunks[chunkNumber];
  return chunk;
}

function getNearbyChunks(chunks, chunkNumber, range) {
  const nearbyChunks = [];
  for (let i = chunkNumber - range; i < chunkNumber + range; i++) {
    const chunk = chunks[i];
    if (chunk) {
      nearbyChunks.push(chunk);
    }
  }
  return nearbyChunks;
}

async function getAudioBuffer(url) {
  console.log(`Fetching audio buffer from ${url}...`);
  const request = new Request(url);
  const response = await fetch(request);
  const audioData = await response.arrayBuffer();
  console.log('Decoding audio...');
  const audioCtx = new AudioContext();
  const buffer = await audioCtx.decodeAudioData(audioData);
  console.log(`Fetched ${Math.round(buffer.duration)}(s) of audio.`);
  console.log(`Audio sample rate is ${buffer.sampleRate}hz.`);
  console.log(`Audio has ${buffer.numberOfChannels} channel(s).`);
  console.log(buffer);
  return buffer;
}

function roundTo(number, precision) {
  const places = Math.pow(10, precision);
  return Math.round(number * places) / places;
}

function appendFirst(parent, newChild) {
  if (parent.firstChild) {
    parent.insertBefore(newChild, parent.firstChild);
  } else {
    parent.appendChild(newChild);
  }
}

/**
 * @typedef {Object} JumpCutterOptions
 * @property {number} silentThreshold
 * @property {number} silentSpeed
 * @property {number} noiseSpeed
 * @property {boolean} silentOnly
 */

class JumpCutter {
  /**
   * 
   * @param {HTMLVideoElement} video 
   * @param {string} audioStream 
   * @param {HTMLElement} canvasWrapper
   * @param {JumpCutterOptions} [options]
   */
  constructor(video, audioStream, canvasWrapper, options) {
    this.video = video;
    this.audioStream = audioStream;
    this.canvasWrapper = canvasWrapper;
    this.canvas = null;
    this.ctx = null;
    this.audioBuffer = null;
    this.options = Object.assign({
      silentThreshold: 0.04,
      silentSpeed: 4,
      noiseSpeed: 1,
      silentOnly: false,
      fps: 29.97,
      chunkScale: 4,
    }, options);

    this.lastTime = -1;
  }

  get currentVideoPercentage() {
    return this.video.currentTime / this.video.duration;
  }

  addCanvas(width = this.canvasWrapper.clientWidth, height = 50) {
    /** @type {HTMLCanvasElement} */
    const canvas = document.createElement('canvas');
    // canvas.style.width = '100%';
    canvas.width = width;
    canvas.height = height;
    appendFirst(this.canvasWrapper, canvas);
    return canvas;
  }

  drawScrubber(worldWidth, worldHeight) {
    const position = this.currentVideoPercentage * worldWidth;
    this.ctx.strokeStyle = '#fff';
    this.ctx.beginPath();
    this.ctx.moveTo(position, 0);
    this.ctx.lineTo(position, worldHeight);
    this.ctx.stroke();
  }

  drawThreshold(worldHeight) {
    let y = worldHeight - (this.options.silentThreshold * worldHeight);
    this.ctx.strokeStyle = '#fff';
    this.ctx.beginPath();
    this.ctx.moveTo(0, y);
    this.ctx.lineTo(this.ctx.canvas.width, y);
    this.ctx.stroke();
  }

  animationLoop(render) {
    const time = this.video.currentTime;
    if (time !== this.lastTime) {
      const timeDiff = time - this.lastTime;
      this.ctx.fillStyle = '#121212';
      this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
      render(timeDiff);
    }
    this.lastTime = time;
    requestAnimationFrame(() => this.animationLoop(render));
  }

  chunkIsSilent(chunk) {
    if (this.options.silentOnly) {
      return chunk > this.options.silentThreshold;
    } else {
      return chunk < this.options.silentThreshold;
    }
  }

  // TODO: Rename me
  async processVideo() {
    try {
      this.audioBuffer = await getAudioBuffer(this.audioStream);

      // TODO: Figure out what this is and rename it;
      const videoDurationScale = 100;
      const worldWidth = this.video.duration * videoDurationScale;
      console.log(worldWidth, this.audioBuffer.length);
      // const worldWidth = this.audioBuffer.length;
      const worldHeight = 50;

      this.canvas = this.addCanvas();
      this.ctx = this.canvas.getContext('2d');
      this.ctx.lineWidth = 1;

      // TODO: Figure out how to get actual FPS
      const fps = this.options.fps;
      const chunkSize = (this.audioBuffer.sampleRate / fps) * this.options.chunkScale;
      // const chunkSize = this.audioBuffer.length / (fps * this.video.duration);
      const chunks = getBins(this.audioBuffer, chunkSize);
      const worldChunkSize = worldWidth / chunks.length; // FIXME: Figure this out more smartly
      const timeBasedChunkSize = roundTo(this.audioBuffer.duration / chunks.length, 6); // TODO: Name this better
      const chunkGraph = new LineGraph(this.ctx, worldWidth, worldHeight, chunks, (value) => {
        if (value > this.options.silentThreshold) {
          return '#0f0';
        } else {
          return '#f00';
        }
      });

      /* this.ctx.save();
      this.ctx.translate((chunkNumber * 1.0 / chunks.length) * worldWidth, 0);
      this.ctx.fillStyle = strokeStyle;
      this.ctx.fillRect(0, 0, worldChunkSize, worldHeight / 2);
      this.ctx.restore(); */

      const updateVideoSpeed = () => {
        let chunkNumber = Math.floor(this.currentVideoPercentage * chunks.length);
        let currentChunk = chunks[chunkNumber];
        let doSearch = false;
        if (this.chunkIsSilent(currentChunk)) {
          doSearch = true;
        }
        if (!doSearch) {
          chunkNumber++;
          let nextChunk = chunks[chunkNumber];
          if (this.chunkIsSilent(nextChunk)) {
            doSearch = true;
          }
        }
        // If currently in a quiet section,
        if (doSearch) {
          // find next non-quiet chunk.
          do {
            chunkNumber++;
            const chunk = chunks[chunkNumber];
            if (!this.chunkIsSilent(chunk)) {
              // Put some padding around the jump for the browser to catch up
              let moveToChunk = chunkNumber - 0.5;
              let newTime = (moveToChunk / chunks.length) * this.video.duration;
              // Video current time precision is 6 decimal places. If we don't round the time,
              // we can't accurately compare the generated one with the real one.
              newTime = roundTo(newTime, 6);
              if (newTime > this.video.currentTime) {
                this.ctx.save();
                this.ctx.translate((newTime / this.video.duration) * worldWidth, 0);
                this.ctx.beginPath();
                this.ctx.moveTo(0, 0);
                this.ctx.lineTo(0, this.canvas.height);
                this.ctx.stroke();
                this.ctx.restore();

                if (!this.video.paused) {
                  this.video.currentTime = newTime;
                }
                break;
              }
            }
          } while (chunkNumber < chunks.length);
        }
      }

      this.animationLoop((timeDiff) => {
        this.ctx.save();
        // Center waveform
        this.drawThreshold(worldHeight);
        this.ctx.translate(this.canvas.width / 2.0, 0);

        this.ctx.strokeStyle = '#fff';
        this.ctx.beginPath();
        this.ctx.moveTo(0, 0);
        this.ctx.lineTo(0, this.canvas.height);
        this.ctx.stroke();
        // Slide waveform to the left over time
        // this.ctx.translate(-this.currentVideoPercentage * worldWidth, 0);
        // TODO: Simplify drawing scrubber now that it is stationary
        // this.drawScrubber(worldWidth, worldHeight);
        this.ctx.restore();
        chunkGraph.position.x = this.canvas.width / 2.0 + (-this.currentVideoPercentage * worldWidth);
        chunkGraph.render();
        updateVideoSpeed();
      });
    } catch (error) {
      console.log('error :', error);
    }
  }
}

/**
 * 
 * @param {JumpCutterOptions} options
 */
function buildGui(options) {
  const gui = new dat.GUI({
    autoPlace: false,
  });
  gui.add(options, 'silentThreshold', 0, 1);
  gui.add(options, 'silentSpeed', 0);
  gui.add(options, 'noiseSpeed', 0);
  gui.add(options, 'silentOnly');
  gui.add(options, 'fps', 0);
  gui.add(options, 'chunkScale');

  /** @type {HTMLElement} */
  const playerWrapper = document.querySelector('#primary-inner > #player');
  playerWrapper.style.position = 'relative';

  const guiWrapper = document.createElement('div');
  guiWrapper.style.position = 'absolute';
  guiWrapper.style.right = '0';
  guiWrapper.style.top = '0';
  guiWrapper.appendChild(gui.domElement);

  playerWrapper.appendChild(guiWrapper)
}

console.log('Jumpcutter extension loaded');

// FIXME: Set to true if building as extension
const extension = true;

async function main() {
  /** @type {HTMLElement} */
  let canvasWrapper;
  if (extension) {
    canvasWrapper = document.createElement('div');
    canvasWrapper.id = 'canvas-wrapper';
  } else {
    canvasWrapper = document.querySelector('#canvas-wrapper');
  }
  const video = document.querySelector('video');
  const jumpCutter = new JumpCutter(video, null, canvasWrapper);

  buildGui(jumpCutter.options);

  if (extension) {
    jumpCutter.audioStream = await YouTubeAudio.getAudioUrlOfCurrentVideo();
    appendFirst(document.querySelector('#info'), canvasWrapper);
    jumpCutter.processVideo();
  } else {
    const input = document.querySelector('#video-file-input');
    input.addEventListener('change', (ev) => {
      const [file] = input.files;
      let source = video.querySelector('source');
      let sourceElExists = source === null;
      if (sourceElExists) {
        source = document.createElement('source');
      }
      source.src = URL.createObjectURL(file);
      source.type = file.type;
      if (sourceElExists) {
        video.appendChild(source);
      }
    });
    video.addEventListener('loadeddata', () => {
      jumpCutter.audioStream = video.currentSrc;
      jumpCutter.processVideo();
    });
  }
}

if (extension) {
  main();
} else {
  window.addEventListener('load', main, false);
}